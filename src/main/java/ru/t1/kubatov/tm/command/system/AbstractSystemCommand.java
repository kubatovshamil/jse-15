package ru.t1.kubatov.tm.command.system;

import ru.t1.kubatov.tm.api.service.ICommandService;
import ru.t1.kubatov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
