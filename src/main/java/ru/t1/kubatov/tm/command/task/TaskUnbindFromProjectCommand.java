package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Unbind Task from Project.";

    public final static String NAME = "task-unbind-from-project";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("Enter Project ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

}
