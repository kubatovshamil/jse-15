package ru.t1.kubatov.tm.command.project;

import ru.t1.kubatov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Update Project by index.";

    public final static String NAME = "project-update-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(index, name, description);
    }

}
