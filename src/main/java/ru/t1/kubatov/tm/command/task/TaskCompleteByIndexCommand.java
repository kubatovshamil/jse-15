package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Complete Task by index.";

    public final static String NAME = "task-complete-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }

}
