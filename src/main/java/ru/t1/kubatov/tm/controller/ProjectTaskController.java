package ru.t1.kubatov.tm.controller;

import ru.t1.kubatov.tm.api.controller.IProjectTaskController;
import ru.t1.kubatov.tm.api.service.IProjectTaskService;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter Project ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("Enter Project ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId, taskId);
    }

}
